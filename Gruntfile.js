module.exports = function(grunt){

    grunt.initConfig({

        // Let's combine all our JS files into one
        concat: {
            // Front-end
            main: {
                src: [
                        'js/slick.js',
                        'js/scripts.js'
                      ],
                dest: 'js/concat/slider-scripts.js',
            }
        },
        // Let's minimize our JS files
        uglify: {
            // Front-end
            main: {
                src: 'js/concat/slider-scripts.js',
                dest: 'js/min/slider.min.js'
            }
        },
        // Let's provide our users a minified version of all our CSS files
        // This is done when you're ready to provide a final version copy
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'css/',
                    src: ['*.css', '!*.min.css'],
                    dest: 'css/',
                    ext: '.min.css'
                }]
            }
        },

        // This creates a clean WP theme copy to use in production
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        src: [
                                './*.php',
                                './README.md',
                                './CHANGELOG.md',
                                'classes/*.php',
                                'css/**.css',
                                'css/!*.map',
                                'images/**',
                                'js/**'
                            ],
                        dest: 'dist/'
                    }
                ]
            }
        },
        // Let's zip up our /dist (production wordpress theme)
        // Change version number in style.css
        compress: {
            main: {
                options: {
                    archive: 'tcu_slider.2.3.4.zip'
                },
                files: [
                        {
                            expand: true,
                            cwd: 'dist/',
                            src: ['**'],
                            dest: 'tcu_slider/'
                        }
                    ]

            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compress');

    // Development workflow
    grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);

    // Build copy to production directory
    grunt.registerTask('build', ['copy']);

    // Zip our clean directory to easily install in WP
    grunt.registerTask('zip', ['compress']);

};