<?php
/**
 * Exit if access directly
 **/
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Registers our post type
 * Adds our custom meta boxes
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 **/
class TCU_Create_Slider
{

    /**
     * Post type name
     **/
    protected static $post_type_name = "tcu_slider";

    /**
     * user capabilities
     */
    private static $cap_aliases = array(
        'editor' => array( // full permissions to a post type
            'read',
            'read_private_posts',
            'edit_posts',
            'edit_others_posts',
            'edit_private_posts',
            'edit_published_posts',
            'delete_posts',
            'delete_others_posts',
            'delete_private_posts',
            'delete_published_posts',
            'publish_posts',
        ),
        'author' => array( // full permissions for content the user created
            'read',
            'edit_posts',
            'edit_published_posts',
            'delete_posts',
            'delete_published_posts',
            'publish_posts',
        ),
        'contributor' => array( // create, but not publish
            'read',
            'edit_posts',
            'delete_posts',
            'upload_files'
        ),
        'subscriber' => array( // read only
            'read',
        ),
    );

    /**
     * Our constructor
     *
     * @return void
     **/
    public function __construct()
    {
        if (!post_type_exists(self::$post_type_name)) {
            add_action('init', array( &$this, 'register_slider_post_type' ));

            add_action('wp_loaded', array(__CLASS__, 'tcu_set_initial_caps'));
        }
    }

    /**
     * Returns post type name
     *
     * @return $post_type_name string The name of the custom post type
     **/
    public static function get_post_type_name()
    {
        return self::$post_type_name;
    }

    /**
     * Register our Post Type
     *
     * @return void
     **/
    public function register_slider_post_type()
    {
        register_post_type(self::$post_type_name,

            // let's now add all the options for this post type
            array(
                'labels'             => array(
                'name'               => __('Sliders', 'tcu_slider'),
                'singular_name'      => __('Slider', 'tcu_slider'),
                'all_items'          => __('All Sliders', 'tcu_slider'),
                'add_new'            => __('Add New', 'tcu_slider'),
                'add_new_item'       => __('Add New Slider', 'tcu_slider'),
                'edit'               => __('Edit', 'tcu_slider'),
                'edit_item'          => __('Edit Slider', 'tcu_slider'),
                'new_item'           => __('New Slider', 'tcu_slider'),
                'view_item'          => __('View Slider', 'tcu_slider'),
                'search_items'       => __('Search Sliders', 'tcu_slider'),
                'not_found'          => __('Nothing found in the Database.', 'tcu_slider'),
                'not_found_in_trash' => __('Nothing found in Trash', 'tcu_slider')
                ),
                'description'         => __('Easily add a slider with HTML content', 'tcu_slider'),
                'public'              => false,
                'has_archive'         => false,
                'publicly_queryable'  => true,
                'show_ui'             => true,
                'supports'            => array('title'),
                'menu_position'       => 20,
                'menu_icon'           => 'dashicons-images-alt',
                'exclude_from_search' => true,
                'capability_type'     => array('tcu_slider', 'tcu_sliders'),
                'map_meta_cap'        => true
            )

        ); /* end of register post type */
    }

    /**
     * Grant caps for the given post type to the given role
     *
     * @param string $post_type The post type to grant caps for
     * @param string $role_id The role receiving the caps
     * @param string $level The capability level to grant (see the list of caps above)
     *
     * @return bool false if the action failed for some reason, otherwise true
     */
    public static function register_post_type_caps($post_type, $role_id, $level = '')
    {
        if (empty($level)) {
        $level = $role_id;
        }

        if ('administrator' === $level) {
            $level = 'editor';
        }

        if (! isset( self::$cap_aliases[$level])) {
            return false;
        }

        $role = get_role($role_id);
        if (! $role) {
            return false;
        }

        $pto = get_post_type_object( $post_type );
        if (empty($pto)) {
            return false;
        }

        foreach (self::$cap_aliases[ $level ] as $alias) {
            if (isset($pto->cap->$alias)) {
                $role->add_cap($pto->cap->$alias);
            }
        }

        return true;
    }

    /**
     * Grant caps for the given post type to the given role
     *
     * @param string $post_type The post type to grant caps for
     * @param string $role_id The role receiving the caps
     * @param string $level The capability level to grant (see the list of caps above)
     *
     * @return bool false if the action failed for some reason, otherwise true
     */
    public static function tcu_set_initial_caps()
    {
        foreach (array( 'administrator', 'editor', 'author', 'contributor', 'subscriber' ) as $role) {
           self::register_post_type_caps(self::$post_type_name, $role);
        }
    }

    /**
     * Remove all caps for the given post type from the given role
     *
     * @param string $post_type The post type to remove caps for
     * @param string $role_id The role which is losing caps
     *
     * @return bool false if the action failed for some reason, otherwise true
     */
    public static function remove_post_type_caps($post_type, $role_id)
    {

        $role = get_role($role_id);
        if ( ! $role ) {
            return false;
        }

        foreach ($role->capabilities as $cap => $has) {
            if (strpos( $cap, $post_type ) !== false) {
                $role->remove_cap( $cap );
            }
        }

        return true;
    }

    /**
     * Remove capabilities for events and related post types from default roles
     *
     * @return void
     */
    public static function remove_all_caps()
    {
        foreach (array( 'administrator', 'editor', 'author', 'contributor', 'subscriber' ) as $role) {
           self::remove_post_type_caps(self::$post_type_name, $role);
        }
    }

}
