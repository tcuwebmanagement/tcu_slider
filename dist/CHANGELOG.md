# TCU Slider Changelog

Author: Mayra Perales <m.j.perales@tcu.edu>

*******************************************************************

**v2.3.4**

- Fixed user capabilities

**v2.3.4**

- Deleted the Auto-update class
- Added md5 hash to mask update URL

**v1.3.3**

- Added capabilities to post type
- Fixed PHP code to meet PSR standards

**v1.2.3**

- Fix 404 error for ajax-loader.gif
- Added version number to CSS/JS

**v1.2.2**

- Added update class

**v1.1.2**

- Removed ACF JSON files
- Added ACF PHP include files

**v1.1.1**

- changed all_sliders() method to public
- added adaptiveHeight to slider
- updated acf file to allow visual and text editor
- removed unnecessary cf class

**v1.0.1**

- Added more padding to content area
- removed title metabox
- changed content textarea to wp_editor
- Added tcu-button transparent styling w/ yellow hover
- increased the slider height to 550px for desktop view

**v1.0.0**

- initial commit
- developed slider
