<?php
/*
    Plugin Name: TCU Slider
    Description: Easily add a slider into the homepage. Warning: this plugin will not work without the TCU Web Standards theme and the Advance Custom Field Pro plugin.
    Version: 2.3.4
    Author: Website & Social Media Management
    Author URI: http://mkc.tcu.edu/web-management.asp

    License: GNU General Public License v2.0
    License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

/**
 * Exit if accessed directly
 **/
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Let's get started!
 **/
if (class_exists('TCU_Register_Slider')) {
    new TCU_Register_Slider;
}

/**
 * Registers plugin and loads functionality
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 *
 **/
class TCU_Register_Slider
{

    /**
     * Our plugin version
     *
     */
    protected static $version = "2.3.4";

    /**
     * Our slider object
     *
     **/
    protected $slider;

    /**
     * Our display_slider object
     *
     **/
    protected $display_slider;

    /**
     * Our constructor
     *
     * @return void
     **/
    public function __construct()
    {

        // Loads all our include files
        $this->load_files();

        // Activation and uninstall hooks
        register_activation_hook(__FILE__, array( &$this, 'activate_me' ));
        register_deactivation_hook(__FILE__, array( $this, 'uninstall_me' ));

        // load all our css and js files on front end side
        add_action('wp_enqueue_scripts', array( $this, 'register_front_end_scripts' ), 999);

        // image size
        add_image_size('tcu-slider', 900, 595, true);

        // Instatiate TCU_Create_Slider
        $this->create_slider();

        // Instatiate TCU_Display_Slider
        $this->display_slider();

        // Instatiate TCU_Update_Slider_Plugin
        $this->update_slider();
    }

    /**
     * Return plugin version
     *
     * @return $version string Plugin version
     **/
    public static function get_version()
    {
        return self::$version;
    }

    /**
     * Instantiate TCU_Create_Slider
     *
     * @return $slider object Slider Custom Post Type
     **/
    public function create_slider()
    {
        $this->slider = new TCU_Create_Slider;
        return $this->slider;
    }

    /**
     * Instantiate TCU_Display_Slider
     *
     * @return $display_slider object Slider HTML
     **/
    public function display_slider()
    {
        $this->display_slider = new TCU_Display_Slider;
        return $this->display_slider;
    }

    /**
     * Instantiate TCU_Update_Slider_Plugin
     *
     * @return $display_slider object Slider HTML
     **/
    public function update_slider()
    {
        $this->update_slider = new TCU_Update_Slider_Plugin;
        return $this->update_slider;
    }

    /**
     * Activate our plugin
     * Check if TCU Web Standards theme is installed
     *
     * @return void
     **/
    public function activate_me()
    {

        // Deactivate plugin if TCU Web Standard Theme is not installed & ACF plugin is not installed
        if (!$this->check_theme() && function_exists('acf_add_local_field_group')) {
            deactivate_plugins(plugin_basename(__FILE__));
            wp_die(__(sprintf('Sorry, but your theme does not support this plugin. Please install the TCU Web Standards Theme and the Advance Custom Fields Pro plugin.')));
            return false;
        }

        // Set user capabilities
        TCU_Create_Slider::tcu_set_initial_caps();

        // Flush our rewrites
        flush_rewrite_rules();
    }

    /**
     * Deactivate plugin
     *
     * @return void
     **/
    public function uninstall_me()
    {
        // Remove user capabilities
        TCU_Create_Slider::remove_all_caps();

        // Flush our rewrites
        flush_rewrite_rules();
    }

    /**
     * Load all our dependencies
     *
     * @return void
     **/
    public function load_files()
    {
        foreach (glob(plugin_dir_path(__FILE__) . '/classes/*.php') as $file) {
            require_once($file);
        }
    }

    /**
     * Check if TCU Web Standards theme is active
     *
     * @return bool True/False
     */
    public function check_theme()
    {

        // Let's get the current theme
        $theme = wp_get_theme();

        // Check if the current/parent theme is TCU Web Standards
        if (('TCU Web Standards' == $theme->name) || ('TCU Web Standards' == $theme->parent_theme)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Register our front end scripts
     *
     *
     * @return void
     **/
    public function register_front_end_scripts()
    {
        wp_register_style('tcuslider-css', plugins_url('/css/slick.min.css', __FILE__), array(), self::$version, 'all');
        wp_register_script('tcuslider-js', plugins_url('/js/min/slider.min.js', __FILE__), array('jquery' ), self::$version, true);


        wp_enqueue_style('tcuslider-css');
        wp_enqueue_script('tcuslider-js');
    }
}
