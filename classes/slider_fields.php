<?php
/**
 * ACF Slider Fields
 */

add_action('acf/init', 'tcu_slider_fields');

function tcu_slider_fields()
{
    acf_add_local_field_group(array(
        'key' => 'group_582b3a468042f',
        'title' => 'Slider',
        'fields' => array(
            array(
                'key' => 'field_582b3a4d12a48',
                'label' => 'TCU Slider',
                'name' => 'tcu_slider_repeater',
                'type' => 'repeater',
                'instructions' => 'Add a new slide by clicking the Add Slide button.',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'min' => 0,
                'max' => 0,
                'layout' => 'row',
                'button_label' => '+ Add Slide',
                'collapsed' => 'field_582b3b3e12a4c',
                'sub_fields' => array(
                    array(
                        'key' => 'field_582b3b3e12a4c',
                        'label' => 'Image',
                        'name' => 'slider_image',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 1,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'url',
                        'preview_size' => 'tcu-thumb-600',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => 'jpg, jpeg',
                    ),
                    array(
                        'key' => 'field_582b3ac812a4a',
                        'label' => 'Content',
                        'name' => 'slider_content',
                        'type' => 'wysiwyg',
                        'instructions' => '',
                        'required' => 1,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'tabs' => 'all',
                        'toolbar' => 'full',
                        'media_upload' => 0,
                        'delay' => 0,
                    ),
                    array(
                        'key' => 'field_582b3b1012a4b',
                        'label' => 'Link',
                        'name' => 'slider_link',
                        'type' => 'url',
                        'instructions' => '',
                        'required' => 1,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                    ),
                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'tcu_slider',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
}
