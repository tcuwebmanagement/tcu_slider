<?php
/**
 * Exit if accessed directly
 **/
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Alias for displaying a slider shortcode
 *
 * @param $id int The slider's ID
 **/
if (! function_exists('tcu_display_slider')) {
    function tcu_display_slider($id)
    {
        echo do_shortcode('[tcu_slider id="' . $id . '"]');
    }
}
