<?php
/**
 * Exit if access directly
 **/
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Display our slider
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 **/
class TCU_Display_Slider
{

    /**
     *
     *
     **/
    public function __construct()
    {

        // [tcu_slider id="1"]
        add_shortcode('tcu_slider', array( $this, 'create_shortcode' ));

        // print thickbox
        add_action('admin_footer', array( $this, 'media_thickbox' ));

        // print media button
        add_action('media_buttons', array( $this, 'media_button' ), 999);
    }

    /**
     * Let's get our object set as an array
     *
     **/
    public static function set_array($data)
    {
        foreach ($data as $key => $value) {
            $key = $value;
        }

        return $data;
    }

    /**
     * Query tcu_slider post type
     *
     * @param $args array Arguments to be merged with the post query
     *
     * @return WP_Query object Post query
     **/
    protected static function query_slider($args = array())
    {
        $slider = array();

        // Our default query arguments
        $defaults = array(
            'posts_per_page'=> -1,
            'offset'        => 0,
            'post_status'   => 'publish',
            'post_type'     => TCU_Create_Slider::get_post_type_name(),
            'orderby'       => 'ID',
            'order'         => 'ASC',
            'no_found_rows' => true
        );

        // Merge the query arguments
        $args = array_merge((array) $defaults, (array) $args);

        // Execute the query
        $query = get_posts($args);

        return $query;
    }

    /**
     * Get all our sliders
     *
     * @return $output array All our sliders
     **/
    public static function all_sliders()
    {

        // Query our post type
        $sliders = self::query_slider();

        $output = array();

        // Loop through each slider
        foreach ($sliders as $post) {

            // Set array
            $slider = self::set_array(get_object_vars($post));
            $output[] = $slider;
        }

        wp_reset_postdata();

        return $output;
    }

    /**
     * Grab our slider by post ID
     *
     * @param $id int The slider/post ID
     *
     * @return $output array Slider by ID
     **/
    public function get_by_id($id)
    {
        $post = get_post($id);

        // Check if post exists
        if (!$post) {
            return false;
        }

        // Check if we are using the correct post type
        if ($post->post_type != TCU_Create_Slider::get_post_type_name()) {
            return false;
        }

        $output = array();

        // Set up our array
        $slider = self::set_array(get_object_vars($post));
        $output[] = $slider;

        return $output;
    }

    /**
     * Make sure auto embed works inside our Visual Editor
     *
     * @param $content string The content within each $post
     * @return $content string Embeded content
     **/
    public function auto_embed($content)
    {
        if (isset($GLOBALS['wp_embed'])) {
            $content = $GLOBALS['wp_embed']->autoembed($content);
        }

        return $content;
    }

    /**
     * Render our slider's HTML
     *
     * @param $slider array WP_Query of our slider by ID
     * @return $html string The HTML shell
     **/
    public function render_html($slider)
    {
        $key = key($slider);
        $slider = get_field('tcu_slider_repeater', $slider[$key]['ID']);

        if ($slider) {
            $html  = '<div class="tcu-slider-wrapper">';
            $html .= '<div class="tcu-slider">';

            foreach ($slider as $slide) {
                $html .= '<div class="tcu-slider-body">';
                $html .= '<div class="tcu-slider-img" style="background-image: url('.esc_url($slide['slider_image']).');"></div>';
                $html .= '<div class="tcu-slider-content">';
                $html .=  wp_kses_post($slide['slider_content']);
                $html .= '<a class="tcu-button tcu-button--transparent tcu-bounce tcu-bounce--right--yellow" href="'.esc_url($slide['slider_link']).'">Read More</a>';
                $html .= '</div>';
                $html .= '</div>';
            }

            $html .= '</div>';
            $html .= '<div class="tcu-slider-controls"></div>';
            $html .= '</div>';
        } else {
            $html = '<div>Oops nothing was found!</div>';
        }

        return $html;
    }

    /**
     * Creates our shortcode
     *
     * @param $atts array The slider ID
     * @return $html string Slider shortcode
     **/
    public function create_shortcode($atts)
    {
        extract(shortcode_atts(array( 'id' => false ), $atts));

        if (!empty($id)) {
            $slider = self::get_by_id($id);
            $html = self::render_html($slider);
        }

        return $html;
    }

    /**
     * Prints our "ADD Slider" Media Thickbox
     *
     * @return void
     **/
    public function media_thickbox()
    {
        global $pagenow;

        if (('post.php' || 'post-new.php') != $pagenow) {
            return;
        }

        // let's get all our sliders
        $sliders = self::all_sliders(); ?>

        <style type="text/css">
            .section {
                padding: 15px 15px 0 15px;
            }
        </style>

        <script type="text/javascript">
            /**
             * Send shortcode to the editor
             */

            var insertSlider = function() {

                var id = jQuery('#tcuslider_thickbox').val();

                // alert if no slider was selected
                if( '-1' === id ) {
                    return alert( tcuSliderContent.alert );
                }

                // Send shortcode to editor
                send_to_editor( '[tcu_slider id="' + id + '"]' );

                // close thickbox
                tb_remove();
             };
        </script>

        <div id="select-tcu-slider" style="display: none;">
            <div class="section">
                <h2><?php _e('Add Slider', 'tcu_slider'); ?></h2>
                <span><?php _e('Select a slider to insert from the box below.', 'tcu_slider'); ?></span>
            </div>

            <div class="section">
                <select name="tcu_slider_name" id="tcuslider_thickbox">
                    <option value="-1">
                        <?php _e('Select Slider', 'tcu_slider'); ?>
                    </option>
                    <?php
                        foreach ($sliders as $slide) {
                            echo "<option value=\"{$slide['ID']}\">{$slide['post_title']} (ID #{$slide['ID']})</option>";
                        } ?>
                </select>
            </div>

            <div class="section">
                <button id="insert-slider" class="button-primary" onClick="insertSlider();"><?php _e('Insert Slider', 'tcu_slider'); ?></button>
                <button id="close-slider-thickbox" class="button-secondary" style="margin-left: 5px;" onClick="tb_remove();"><a><?php _e('Close', 'tcu_slider'); ?></a></button>
            </div>
        </div>
    <?php
    }

    /**
     * Media button
     *
     * @param $editor_id int Editor ID
     **/
    public function media_button($editor_id)
    {
        ?>

        <style type="text/css">
            .wp-media-buttons .insert-slider span.wp-media-buttons-icon {
                margin-top: -2px;
            }
            .wp-media-buttons .insert-slider span.wp-media-buttons-icon:before {
                content: "\f175";
                font: 400 16px/1 dashicons;
                speak: none;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }
        </style>

        <a href="#TB_inline?width=480&amp;inlineId=select-tcu-slider" class="button thickbox insert-slider" data-editor="<?php echo esc_attr($editor_id); ?>" title="<?php _e('Add Slider', 'tcu_slider'); ?>">
            <span class="dashicons dashicons-images-alt"></span><?php _e(' Add Slider', 'tcu_slider'); ?>
        </a>

    <?php
    }
}
?>