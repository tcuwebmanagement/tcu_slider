jQuery(document).ready(function($){
    $('.tcu-slider').slick({
        dots: true,
        arrows: true,
        infinite: true,
        nextArrow: '<span class="slick-arrow slick-arrow-right">&gt;</span>',
        prevArrow: '<span class="slick-arrow slick-arrow-left">&lt;</span>',
        appendDots: '.tcu-slider-controls',
        appendArrows: '.tcu-slider-controls',
        mobileFirst: true,
        adaptiveHeight: true
    });
});